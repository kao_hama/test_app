class StaticPagesController < ApplicationController
  def index
    @informations = Information.all
  end

  def show
    @information = Information.find(params[:id])
  end

  def new
    @information = Information.new
  end

  def create
    @information = Information.new(information_params)
    if @information.save
      redirect_to information_path(@information)
    else
      render 'informations/new'
    end
  end

  def destroy
  end

  private

    def information_params
      params.require(:information).permit(:id, :title, :content, :created_at)
    end

end
