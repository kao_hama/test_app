class InformationsController < ApplicationController
	def index
		@informations = Information.all
	end

	def show
		@information = Information.find(params[:id])
	end

	def new
		@information = Information.new
	end

	def create
		@information = Information.new(information_params)
		if @information.save
			redirect_to information_path(@information)
		else
			render 'informations/new'
		end
	end

	def edit
		@information = Information.find(params[:id])
	end

	def update
		@information = Information.find(params[:id])
		if @information.update_attributes(information_params)
			redirect_to information_path(@information)
		else
			render 'informations'
		end
	end

	def destroy
		Information.find(params[:id]).destroy
		flash[:success] = "記事をポイしました"
		redirect_to informations_path
	end

	private

		def information_params
			params.require(:information).permit(:id, :title, :content, :created_at)
		end
end
